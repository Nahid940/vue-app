import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import HomePage from '@/components/HomePage.vue';
import StudentList from '@/components/StudentList.vue';

export default new VueRouter({
    routes:[
        {path:'/',component:HomePage},
        {path:'/list',component:StudentList},
    ],
    mode:'history'
})

    
