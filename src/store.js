import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export const store=new Vuex.Store({
    state:{
        studentlist:[
            {name:'Nahid Islam',age:25},
            {name:'Kamal Islam',age:26},
            {name:'X Islam',age:16},
            {name:'Y Islam',age:20},
            {name:'Z Islam',age:25},
            {name:'AS Islam',age:25},
        ]
    },
    getters:{
        studentLis(state)
        {
            return state.studentlist
        },

        totalStudent(state){
            return state.studentlist.length
        }
    }
})