import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './routes';
import {store} from './store'



// const router=new VueRouter({
//   router:Routes,
//   mode:'history'
// })

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
